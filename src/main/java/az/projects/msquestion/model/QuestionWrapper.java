package az.projects.msquestion.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionWrapper {
    Integer id;
    String questionTitle;
    String option1;
    String option2;
    String option3;
    String option4;
}
